/**********************
Titre:
	Final_Recepteur.c
	
Description:
	Recoie l'information de l'emetteur:
	Si le dernier bit est masqu� � 0, le moteur recoit l'information du voltage par le XBEE
	et change sa vitesse de rotation.
	Sinon si le dernier bit est masqu� � 1, le servomoteur change son angle.
	
Auteurs:
	Equipe 02
***********************/

//Librairies
#include <avr/io.h>

//Constantes
#define F_CPU 2000000 					// horloge du uC
#define UART_BAUD 9600					// vitesse de communication serie en bauds
#define UART_UBRR F_CPU/16/UART_BAUD-1  // valeur a placer dans UBRR pour fixer la vitesse (F_CPU/(16*UART_BAUD)-1)
#define PERIODE	20000					// valeur top du MLI
#define RC		20000					// valeur de comparaison pour un rapport cyclique de 50%

// Prototype
void USART_Init( unsigned int ubrr);
unsigned char USART_Rx(void);
void USART_Tx(unsigned char envoi);


// prototypes pour controle de MLI (PWM)
void MLI_Init(int top);
void MLI_SetA(int limit);
void MLI_SetB(int limit);


// programme principal
int main(void)
{	

	// Definition de la valeur lue par le recepteur
	unsigned char valeur;
	
	// Definition des variables memoires
	int angle, vitesse;
	
    // initialiser le MLI avec la bonne valeur maximale du compteur: TOP
	// TOP obtenu selon la formule Fpwm=FCK/(2*N*TOP) avec N=1, FCK=2MHz
	MLI_Init(PERIODE);
		
	USART_Init(UART_UBRR);		// Appelle de la routine d'initialisation du UART
	
	// Boucle infinie
	while(1){
		
		// Lecture de donn�e recue par le XBee
		valeur = USART_Rx();
		
		// Si le dernier bit est masqu� � 0
		if(bit_is_clear(valeur, 0)){
			
			/**********************
			* Moteur
			**********************/

			// fixer le rapport cyclique en ajustant la limite de comparaison
			MLI_SetB(20000.0/255.0*valeur);
			
		}
		//Sinon si le dernier bit est masqu� � 1
		if(bit_is_set(valeur, 0)){
			
			/***********************
			* Servomoteur
			************************/
		
			// Conversion de la valeur lue
			angle = (valeur*3.9216+1000);
		
			// fixer le rapport cyclique en ajustant la limite de comparaison
			MLI_SetA(angle);
			
		}

	}	
	
}


/***************
Fonction : USART_Init
Description: Initialisation de la communication serie
 	la fonction defini la vitesse de communication dans UBRR,
 	active la reception dans UCSRB, et defini un format de mot
	avec 8 bits de donnees et 1 bit d'arret.
Parametre:
	ubrr: valeur a placer dans le registre UBRR pour fixer la vitesse  
Valeur de retour: 
	aucune
***************/
void USART_Init( unsigned int ubrr)
{
	// fixer la vitesse de communication
	UBRRH = (unsigned char)(ubrr>>8);
	UBRRL = (unsigned char)ubrr;
	//activer la reception
	UCSRB = (1<<RXEN)|(1<<TXEN);
	// fixer le format du mot: 8 bits de data, 1 bit de stop 
	UCSRC = (1<<URSEL)|(3<<UCSZ0);
}

/***************
Fonction : USART_Rx
Description: R?ception de donn?es
 	la fonction attends une reception de donn?e en surveillant 
	le bit RXC du registre UCSRA. Quand celui-ci devient 1, 
	un caractere a ete recu et se trouve dans UDR. Le contenu de UDR est
	renvoye. 
Parametre:
	aucun
Valeur de retour:
	Le caractere recu.
***************/
unsigned char USART_Rx(void)
{
	while(!(UCSRA & (1<<RXC))); // attendre le drapeau indiquant la reception d'un car.
	return UDR;					// renvoyer la valeur recu (contenu du registre UDR)
}


/***************
Fonction : MLI_Init
Description: 
	Initialisation du MLI.
Parametre: 
	top: valeur max que le compteur doit 
	atteindre avant de commencer � "d�compter"
Valeur de retour: 
	aucune
***************/
void MLI_Init(int top)
{	
	// Configuration des broches de sortie
	DDRD|=((1<<PD5)|(1<<PD4));

	// Configuration du compteur et du comparateur
	TCCR1A|=((1<<COM1A1)|(1<<COM1B1));
	TCCR1B|=(1<<WGM13);

	// Configuration de la valeur maximale du compteur 
	ICR1=top;
	
	// Initialiser la valeur du compteur � 0
	TCNT1=0;

	// D�marrer le compteur et fixer un facteur de division de fr�quence
	TCCR1B|=(1<<CS10);
	TCCR1B&=~((1<<CS12)|(1<<CS11));
}



/***************
Fonction : MLI_SetA
Description: 
	pour le MLI A (broche PD5): Servomoteur
	Fixe le rapport cyclique en ajustant la valeur de comparaison OCR1A 
Parametre: 
	limit: la nouvelle valeur de comparaison
Valeur de retour: 
	aucune
***************/

void MLI_SetA(int limit)
{	
	OCR1A=limit;
	
}

/***************
Fonction : MLI_SetB
Description: 
	pour le MLI B (broche PD4): Moteur
	Fixe le rapport cyclique en ajustant la valeur de comparaison OCR1B 
Parametre: 
	limit: la nouvelle valeur de comparaison
Valeur de retour: 
	aucune
***************/

void MLI_SetB(int limit)
{	
	OCR1B=limit;
}






