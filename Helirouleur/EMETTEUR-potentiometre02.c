/*******************
Titre: 
	Final_Emetteur.c

Description:
	Programme final utilisee pour le microprocesseur ATMEGA32 - emetteur
	
Procedure:
	Pour discerner l'envoi du signal des deux potentiometres, on masque le dernier bit
	par un chiffre lue par la broche. Par exemple, la broche PA0 met le dernier bit
	� 0 et le broche PA1 le met � 1.
	
	Le recepteur filtrera les bits re�us: Si le dernier bit est 0, le moteur change
	sa vitesse de rotation. Sinon si le dernier bit est 1, le servomoteur change
	son angle d'inclinaison.
	
Auteurs:
	Equipe 02

********************/

// Librairies
#include <avr/io.h>
#include <util/delay.h>

// Constantes
#define F_CPU 2000000 	// horloge du uC
#define UART_BAUD 9600	// vitesse de communication serie en bauds
#define UART_UBRR F_CPU/16/UART_BAUD-1  // valeur a placer dans UBRR pour fixer la vitesse (F_CPU/(16*UART_BAUD)-1)

// prototypes
void USART_Init(unsigned int ubrr);
void USART_Tx(unsigned char envoi);


// prototypes de l'ADC
void ADC_Init();
unsigned char ADC_read();


//Debut du programme principal
int main(void)
{
	// tension lue par le convertisseur
	unsigned char volt;
	
	USART_Init(UART_UBRR);			//Appelle de la routine d'initialisation du UART
	
	// Initialisation de l'ADC
	ADC_Init();
	
	// pour toujours (boucle infinie)
	while(1)
	{		

			
		/************************
		*Lecture: Broche PA0 (Moteur)
		*************************/

		// Conversion de la broche PA0 (ADC0) comme entr�e � convertir
		ADMUX&=~(1<<MUX0);
		// lecture de la tension
		volt = ADC_read();
		
		// Transmission de la tension
		// Le dernier bit est masque a '0'
		USART_Tx(volt&=~(1<<0)); // Signal au moteur
		
		// attente de 10 ms pour synchronisation
		_delay_ms(10);
			
			
		/************************
		*Lecture: Broche PA1 (Servomoteur)
		*************************/
			
		// Choisir la broche PA1 (ADC1)
		ADMUX|= (1<<MUX0);
		// lecture de la tension
		volt = ADC_read();
		
		// Envoie de la tension au recepteur
		// Le dernier bit est masque a '1'
		USART_Tx(volt|=(1<<0)); // Signal au servomoteur
		
		// attente de 10 ms pour synchronisation
		_delay_ms(10);


	}
}

/***************
Fonction : USART_Init
Description: Initialisation de la communication serie
 	la fonction defini la vitesse de communication dans UBRR,
 	active la reception dans UCSRB, et defini un format de mot
	avec 8 bits de donnees et 1 bit d'arret.
Parametre:
	ubrr: valeur a placer dans le registre UBRR pour fixer la vitesse  
Valeur de retour: 
	aucune
***************/
void USART_Init( unsigned int ubrr)
{
	// fixer la vitesse de communication
	UBRRH = (unsigned char)(ubrr>>8);
	UBRRL = (unsigned char)ubrr;
	
	//activer la reception
	UCSRB = (1<<RXEN)|(1<<TXEN);
	
	// fixer le format du mot: 8 bits de data, 1 bit de stop 
	UCSRC = (1<<URSEL)|(3<<UCSZ0);
}


/*************
Fonction: USART_Tx
Description: Transmission des donnees en format 'unsigned char'
	(d'une valeur entre 0 a 255).
Parametre:
	envoi: Le signal a envoyer au XBEE selon la lecture du MLI
Valeur de retour:	
	aucune
***************/
void USART_Tx(unsigned char envoi)
{
	while(!(UCSRA & (1<<UDRE))); // attendre le drapeau indiquant canal libre.
	UDR=envoi;
	
}


/***************
Fonction : ADC_Init
Description: 
	Initialisation de l'ADC. Utilisation du bit PA0 (ADC0) comme entr�e analogique
	Le resultat de l'ADC (10 bit) se retrouve dans 2 registre (ADCH et ADCL)
	Deux format de sortie possible: 
		shift a gauche (ADLAR=1) les 8 MSB se retrouvent dans le registre ADCH
		et les 2 LSB dans ADCL. 
		shift a droite (ADLAR=0) les 8 LSB se retrouvent dans le registre ADCL 
		et les 2 MSB dans ADCH
	Si 8 bits de pr�cision suffisent, on peut utiliser le shift a gauche, 
	et lire simplement les 8 MSB dans ADCH
Parametre: 
	aucune
Valeur de retour: 
	aucune
***************/
void ADC_Init(){
	
	// Mettre la broche 0 et 1 en entree
	DDRA&=~((1<<PA0)|(1<<PA1));
	
	// Initialisation de l'ADMUX
	ADMUX&=~((1<<MUX4)|(1<<MUX3)|(1<<MUX2)|(1<<MUX1)|(1<<MUX0));
	
	// S�lectionner la r�f�rence de tension: la tension d'alimentation
	ADMUX&=~(1<<REFS1);
	ADMUX|=(1<<REFS0);

	// Choisir le format du r�sultat de conversion: shift a gauche pour que
	// les 8 MSB se retrouve dans le registre ADCH (ADLAR=1)
	ADMUX|=(1<<ADLAR);

	// Choisir un facteur de division d'horloge (8)
	ADCSRA&=~(1<<ADPS2);
	ADCSRA|=((1<<ADPS1)|(1<<ADPS0));

	// Activer le CAN
	ADCSRA|=(1<<ADEN);

}

/***************
Fonction : ADC_Read
Description: 
	Lecture du resultat de conversion de l'ADC 10 bits
	Seuls les 8 MSB (ADCH) sont lu et renvoy�s 
	On suppose donc que le mode de l'ADC est "shift a gauche"
Parametre: 
	aucune
Valeur de retour: 
	La valeur binaire lue sur l'ADCH en 'unsigned char'
***************/
unsigned char ADC_read(){

	// D�marrage d'une conversion 
	ADCSRA|=(1<<ADSC);
	
	// Attente de la fin de conversion
	while(bit_is_set(ADCSRA,ADSC));

	// Lecture et renvoie du r�sultat
	return ADCH;

}
