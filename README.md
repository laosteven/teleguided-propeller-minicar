# Teleguided Propeller Based Minicar #
-------

## Components ##

Using an **[Atmel STK500](http://www.atmel.com/tools/STK500.aspx)** unit to implement code in C in an **[ATmega32](http://www.atmel.com/devices/atmega32.aspx)** microchip allows us to control the motor speed of our custom made propeller and the orientation of the servomotor with the use of an **[XBEE radio module](https://en.wikipedia.org/wiki/XBee)**.

## Circuitry ##

All the components mentioned in the previous section must fit in a small circuit board which must be designed by the students under **Altium Designer**. These files are available in the project's root folder: one for the receiver and one for the transmitter.

![PCB_view.JPG](https://bitbucket.org/repo/7keBje/images/294034236-PCB_view.JPG)

## Propeller profile ##

The decision of choosing a propeller profile is based on a computed 2D profile in **MatLab** and in 3D under **SolidWorks** which allows us to calculate stress levels and aerodynamic values. 

After comparing a series of propeller profiles, we were able to 3D print it in plastic.

![maxime.JPG](https://bitbucket.org/repo/7keBje/images/3351935238-maxime.JPG)

# Model render #
-------

![derrier.JPG](https://bitbucket.org/repo/7keBje/images/3944772335-derrier.JPG)

# Objectives #
-------
* Build a mobile body from scratch: from soldering to material machining.
* Upon completion, each group of students must compete with three different types of challenge: speed, maneuverability and finally, a race.
* Combine all the topics we've seen since the start of the university program.

# For more information #
-------
Visit the following website: [**Multidisciplinary project** (TCH098)](https://www.etsmtl.ca/Etudiants-actuels/Cheminement-univ-techno/Cours-horaires-chem-univ/Fiche-de-cours?Sigle=TCH098) [*fr*]